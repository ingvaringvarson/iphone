'use strict';

import polyfills from './libraries/polyfills';

import 'jquery';

$(() => {
    polyfills.init();
    // ================ Здесь инициализируем модули =====================
});
